import datetime
from django.utils import timezone
from django.test import TestCase
from .models import Pergunta

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        o método publicada_recentemente precisa retornar FALSE quando se
        tratar de perguntas com data de publicação no futuro.
        """
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24hs_no_passado(self):
        """
        o método publicada_recentemente DEVE retornar False quando se tratar
        de uma data de publicação anterior a 24hs no passado.
        """
        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passado = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):
        """
        o método publicada_recentemente DEVE retornar TRUE quando se tratar de
        uma data de publicação dentro das últimas 24hs.
        """
        data = timezone.now()-datetime.timedelta(hours=23,minutes=59,seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)
