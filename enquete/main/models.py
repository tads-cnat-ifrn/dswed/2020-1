from django.conf import settings
from django.db import models
from django.utils import timezone
import datetime

class Autor(models.Model):
    pseudonimo = models.CharField(max_length = 100, unique=True)
    usuario = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        default=None,
        null=True
    )
    def __str__(self):
        return self.pseudonimo
    def enquetes_por_data_publicacao(self):
        return self.pergunta_set.order_by('data_publicacao')

class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de Publicação')
    autor = models.ForeignKey(
        Autor,
        on_delete=models.CASCADE,
        default=None,
        null=True
    )
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'últimas 24hs?'

class Opcao(models.Model):
    texto = models.CharField(max_length = 200)
    votos = models.IntegerField(default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto
    class Meta:
        ordering = ["texto"]
        verbose_name_plural = "Opções"
