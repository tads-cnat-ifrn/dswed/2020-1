from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.root, name = 'root'),
    path('enquetes/',
        views.IndexView.as_view(), name = 'index'
    ),
    path('enquete/<int:pk>',
        views.DetalhesView.as_view(), name = 'detalhes'
    ),
    path('enquete/<int:pk>/resultado',
        views.ResultadoView.as_view(), name = 'resultado'
    ),
    path('enquete/<int:id_enquete>/votacao',
        views.votacao, name = 'votacao'
    ),
    path('enquetes/autor/<int:pk>/',
        views.EnquetesAutor.as_view(), name = 'enquetes_autor'
    ),
    path('enquete/cadastrar',
        views.nova_enquete, name = 'nova_enquete'
    ),
]