from django.contrib import admin
from .models import Pergunta, Opcao, Autor

class MyAdminSite(admin.AdminSite):
    site_header = 'Aplicação de Enquetes - Administração'

class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['texto']}),
        ('Informações de data', {'fields': ['data_publicacao']}),
        ('Informações de autor', {'fields': ['autor']}),
    ]
    inlines = [OpcaoInline]
    list_display = ('texto', 'id', 'data_publicacao', 'publicada_recentemente')
    list_filter = ['data_publicacao']
    search_fields = ['texto']

admin_site = MyAdminSite(name='myadmin')
admin_site.register(Pergunta, PerguntaAdmin)
admin_site.register(Autor)