from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Pergunta, Opcao, Autor
from django.contrib.auth.decorators import login_required

def root(request):
    return render(request, 'main/root.html', {})

class IndexView(generic.ListView):
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
        ).order_by('-data_publicacao')

@login_required
def nova_enquete(request):
    if request.method == 'GET':
        return render(request, 'main/nova_enquete.html')
    if request.method == 'POST':
        ## Verifica se o usuário logado possui relacionamento com autor
        if not hasattr(request.user, 'autor'):
            return render(request, 'main/nova_enquete.html', {
                'error_message': "Usuário logado não é um autor válido!"
            })
        ## Recupera os parâmetros enviados no formulário
        texto = request.POST['pergunta']
        op1 = request.POST['op1']
        op2 = request.POST['op2']
        op3 = request.POST['op3']
        op4 = request.POST['op4']
        op5 = request.POST['op5']
        ## Verifica se o texto é diferente de vazio
        if texto:
            pergunta = Pergunta(
                texto = texto,
                data_publicacao = timezone.now(),
                autor = request.user.autor
            )
            pergunta.save()
            if op1:
                op = Opcao(texto=op1, pergunta=pergunta)
                op.save()
            if op2:
                op = Opcao(texto=op2, pergunta=pergunta)
                op.save()
            if op3:
                op = Opcao(texto=op3, pergunta=pergunta)
                op.save()
            if op4:
                op = Opcao(texto=op4, pergunta=pergunta)
                op.save()
            if op5:
                op = Opcao(texto=op5, pergunta=pergunta)
                op.save()
            return HttpResponseRedirect(reverse('main:index'))
        else:
            return render(request, 'main/nova_enquete.html', {
                'error_message': "Texto da enquete inválido! Tente novamente."
            })

class DetalhesView(generic.DetailView):
    model = Pergunta
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())

class EnquetesAutor(generic.DetailView):
    model = Autor
    template_name = 'main/enquetes_autor.html'

class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'main/resultado.html'

def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'main/pergunta_detail.html', {
            'pergunta': pergunta,
            'error_message': "Selecione uma opção VÁLIDA!"
        })
    else:
        if request.session.get('pergunta_votada', -1) == pergunta.id:
            return render(request, 'main/pergunta_detail.html', {
            'pergunta': pergunta,
            'error_message': "Já votou nessa enquete, nessa sessão!"
            })
        else:
            opcao_selecionada.votos += 1
            opcao_selecionada.save()
            request.session['pergunta_votada'] = pergunta.id
        return HttpResponseRedirect(
            reverse('main:resultado', args=(pergunta.id,))
        )






