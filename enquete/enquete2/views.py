from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from main.models import Pergunta, Opcao, Autor

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'enquete2/index.html'
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
        ).order_by('-data_publicacao')

class DetalhesView(generic.DetailView):
    template_name = 'enquete2/detalhes.html'
    model = Pergunta
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())

def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'enquete2/detalhes.html', {
            'pergunta': pergunta,
            'error_message': "Selecione uma opção VÁLIDA!"
        })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(
            reverse('enquete2:resultado', args=(pergunta.id,))
        )

class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquete2/resultado.html'

def about(request):
    return render(request, 'enquete2/about.html', {})

def contact(request):
    return render(request, 'enquete2/contact.html', {})

def gallery(request):
    return render(request, 'enquete2/gallery.html', {})

def products(request):
    return render(request, 'enquete2/products.html', {})