from django.urls import path
from . import views

app_name = 'enquete2'
urlpatterns = [
    path('',
        views.IndexView.as_view(), name = 'index'
    ),
    path('<int:pk>',
        views.DetalhesView.as_view(), name = 'detalhes'
    ),
    path('<int:id_enquete>/votacao',
        views.votacao, name = 'votacao'
    ),
    path('<int:pk>/resultado',
        views.ResultadoView.as_view(), name = 'resultado'
    ),


    path('about', views.about, name='about'),
    path('contact', views.contact, name='contact'),
    path('gallery', views.gallery, name='gallery'),
    path('products', views.products, name='products'),
]